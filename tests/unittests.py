"""
These unit tests meant to be run against a live version of cloaken server
Integration testing
"""
import unittest
from unittest.mock import Mock
from unittest.mock import patch
from cloakensdk.client import SyncClient
from cloakensdk import utility
from cloakensdk.resources import Url
import os
import requests
from datetime import datetime

class TestUrl(unittest.TestCase):

    def setUp(self):
        server = os.environ["SERVER_URL"]
        username = os.environ["USERNAME"]
        password = os.environ["PASSWORD"]
        self.client = SyncClient(server_url=server,
                            username=username,
                            password=password)

    def xtest_create_url(self):
        resource = Url(self.client)
        resource.create(url="http://test.com",unshortened_url="http://long.com")
        data = resource.full_request()
        self.assertEqual(data["status"],"Success")

    def test_unshorten(self):
        resource = Url(self.client)
        resource.unshorten("https://cnn.com")
        data = resource.full_request()
        self.assertEqual(data["status"],"Success")
        self.assertEqual(data["response_code"],201)

    def test_bad_url(self):
        resource = Url(self.client)
        resource.unshorten("http://test")
        data = resource.full_request()
        self.assertEqual(data["status"],"Failed")
        self.assertEqual(data["response_code"],400)

    def test_screenshot(self):
        screenshot = utility.RasterizeAndRetrieveImage(self.client,url="http://test.com")
        result = screenshot.get_screenshot()
        self.assertIn("filename",result.keys())

    @patch('cloakensdk.resources.datetime')
    def test_exception_screenshot(self,datetime_patched):
        datetime_patched.utcnow = Mock(return_value = datetime(1901, 12, 21))
        client = Mock()
        session = Mock()
        session.request.return_value = {"status":"failed"}
        client.session = session
        client.expire = datetime.timestamp(datetime(1901, 12, 22))
        client.server_url = "http://test/"
        client.access_token = "XXXXXXXXXXX"
        screenshot = utility.RasterizeAndRetrieveImage(client,url="http://test.com")
        self.assertRaises(utility.RasterizeException,screenshot.get_screenshot)

    def test_exception_timeout(self):
        client = Mock()
        session = Mock()
        session.request.return_value={"status":"running"}
        client.session = session
        client.expire = datetime.timestamp(datetime(1901, 12, 22))
        client.server_url = "http://test/"
        client.access_token = "XXXXXXXXXXX"
        screenshot = utility.RasterizeAndRetrieveImage(client,timeout=5,url="http://test.com")
        self.assertRaises(utility.RasterizeException,screenshot.get_screenshot)




if __name__ == "__main__":
    unittest.main()
